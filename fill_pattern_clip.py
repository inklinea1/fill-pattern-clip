#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
# Fill Pattern Clip - An Inkscape 1.1+ extension
# Bit of a pointless extension that converts bitmap pattern filled objects
# to clipped bitmap objects
# #############################################################################

import inkex
from inkex import Group
from inkex import command
from inkex import load_svg

from lxml import etree

import random
import tempfile
import os
import shutil

def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None

def create_new_group(parent, prefix, mode):
    group_id = str(prefix) + '_' + str(random.randrange(100000, 999999))
    new_group = parent.add(Group.new('#' + group_id))
    new_group.set('inkscape:groupmode', str(mode))
    new_group.attrib['id'] = group_id

    return new_group

def make_temp_svg(self):

    # Make a temp folder and manually create files in that  folder
    # This is a workaround for NamedTempfile and Tempfile flags on Windows.
    global temp_dir
    temp_dir = tempfile.mkdtemp()
    random_filename = str(random.randrange(1000000, 9000000)) + '.svg'
    temp_svg_file_path = os.path.join(temp_dir, random_filename)
    temp_svg_file = open(temp_svg_file_path, 'x')

    # Write the contents of the updated svg to a tempfile to use with command line

    my_svg_string = self.svg.root.tostring().decode("utf-8")
    temp_svg_file.write(my_svg_string)
    return temp_svg_file

def load_temp_svg(svg_filepath):

    path = os.path.dirname(os.path.realpath(__file__))
    return load_svg(os.path.join(path, svg_filepath))


class FillPatternClip(inkex.EffectExtension):

    def effect(self):
        selection_list = self.svg.selection

        if len(selection_list) < 1:
            return

        for element in selection_list:
            if 'fill' in element.style:
                element_fill = element.style['fill']
            else:
                continue
            if 'url' in element_fill:
                element_fill_id = element_fill.split('#')[1].replace(')', '')
                if self.svg.getElementById(element_fill_id).TAG.lower() == 'pattern':
                    None
            else:
                continue

            # Let create a dummy group in layer mode, and move a duplicate of the element there
            # As Pattern to Objects will give two objects path and pattern
            # with unknown names ( but we can find using xpath on the group )

            element_id = element.get_id()
            element_parent = element.getparent()
            dummy_group = create_new_group(element_parent, 'dummy_group', 'layer')

            # Lets duplicate the element and add to the group

            duplicate_element = element.duplicate()
            duplicate_element_id = duplicate_element.get_id()
            dummy_group.append(duplicate_element)


            # Lets make a tempfile for a command line call to operate on

            my_temp_svg_file = make_temp_svg(self)
            my_temp_svg_filename_path = my_temp_svg_file.name

            # Build a formatted string for command line actions ( Pattern to Object )

            # --batch-process ( or --with-gui ) is required if verbs are used in addition to actions
            my_options = '--batch-process'

            my_actions = '--actions='

            my_actions = my_actions + f'select-by-id:{duplicate_element_id}; \
                    ObjectsFromPattern; \
                    FileSave'

            my_actions = my_actions.replace(' ', '')

            inkex.command.inkscape(my_temp_svg_filename_path, my_options, my_actions)

            # Lets load the processed svg file into a temp element tree
            my_temp_svg_file.seek(0)
            loaded_document = load_temp_svg(my_temp_svg_filename_path)

            # And look at the items we have as a result of Pattern to Object in the group we made
            loaded_document_root = loaded_document.getroot()
            processed_svg_element = loaded_document_root.xpath('//svg:svg')[0]
            processed_dummy_group = processed_svg_element.getElementById(dummy_group.get_id())
            processed_dummy_group_contents = processed_dummy_group.xpath('./*')


            path_id = ''
            pattern_id = ''

            for item in processed_dummy_group_contents:
                if item.TAG == 'path':
                    path_id = item.get_id()
                else:
                    pattern_id = item.get_id()

            # Build a formatted string for command line actions ( Clip pattern to path )

            # --batch-process ( or --with-gui ) is required if verbs are used in addition to actions
            my_options = '--batch-process'

            my_actions = '--actions='

            my_actions = my_actions + f'select-by-id:{pattern_id}; \
                    SelectionToBack; \
                    select-by-id:{path_id},{pattern_id}; \
                    ObjectSetClipPath; \
                    FileSave'

            my_actions = my_actions.replace(' ', '')

            inkex.command.inkscape(my_temp_svg_filename_path, my_options, my_actions)

            # Lets load the processed svg file into a temp element tree
            my_temp_svg_file.seek(0)
            loaded_document = load_temp_svg(my_temp_svg_filename_path)

            # And look at the items we have as a result of Pattern to Object in the group we made
            loaded_document_root = loaded_document.getroot()
            processed_svg_element = loaded_document_root.xpath('//svg:svg')[0]
            processed_dummy_group = processed_svg_element.getElementById(dummy_group.get_id())
            processed_dummy_group_contents = processed_dummy_group.xpath('./*')


            # Remove all items in original dummy group

            for item in dummy_group:
                item.delete()

            # Add processed items to dummy_group and copy the clipping path to <defs>

            for item in processed_dummy_group:
                dummy_group.append(item)
                item_clip_path = item.attrib['clip-path']
                item_clip_path_id = item_clip_path.split('#')[1].replace(')', '')
                clip_path_def = processed_svg_element.getElementById(item_clip_path_id)
                self.svg.xpath('//svg:defs')[0].append(clip_path_def)

            # replace the original element with the result

            for item in dummy_group:
                element.getparent().replace(element, item)

            dummy_group.delete()

            try:
                my_temp_svg_file.close()
            except:
                None

        try:

            shutil.rmtree(temp_dir, ignore_errors=True)
        except:
            None


if __name__ == '__main__':
    FillPatternClip().run()
